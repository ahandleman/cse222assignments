

// Heavily based on Bluetooth Low Energy Lock (c) 2014-2015 Don Coleman
// See: https://github.com/MakeBluetooth/ble-lock/blob/master/phonegap/www/js/index.js

//******   Utility functions (not used here yet) ******
function stringToArrayBuffer(str) {
    // assuming 8 bit bytes
    var ret = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        ret[i] = str.charCodeAt(i);
        console.log(ret[i]);
    }
    return ret.buffer;
}
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}


//******   Actual Application Class/Logic ******

// Note:   Calls to object methods use it's variable name (i.e., app.) rather than
//         "this".  JavaScript's "this" variable doesn't follow the same scope/value
//         rules as in less dynamic languages.  (The "this" is variable dynamically
//         refers to the object that calls code rather than the object in which the "this"
//         is used.  For programmers used to other languages the behavior of "this" in
//         callbacks is often unexpected. To avoid confusion and ambiguity, "this" isn't
//         used at all here.

// Naming Conventions used here:
//   User Interface:
//     Functions that update the user interface or process user interface events start with "ui"
//   Event Handlers:
//     Any code that "responds to an event" has a name that includes the word "on" or "On".
//     Functions that begin with "bleOn" are for BLE based events.
//     Functions that begin with "uiOn" are for user interface events (buttons/touches)

// BLE Object function documentation can be found at:
// https://github.com/don/cordova-plugin-ble-central#api

var app = {

    // Initialized the app. Hide content / etc.
    initialize: function() {
        console.log("initialize");


        // Do initial screen configuration.
        // This can be done here because this file is loaded from the HTML file.
        deviceListScreen.hidden = false;
        // Disable the refresh button until the app is completely ready
        refreshButton.disabled = true;

        // Register to be notified when the "device is ready"
        // This delays the execution of any more code until all the Cordova code is loaded.
        // (This file may be loaded before the Cordova.js file is loaded and, consequently,
        //  shouldn't use any of Cordova's features)
        // See: http://cordova.apache.org/docs/en/6.x/cordova/events/events.html#page-toc-source
        document.addEventListener('deviceready', app.onDeviceReady, false);
    },

// **** Callbacks for application "lifecycle" events. These respond to significant events when the App runs ******

    // the device is ready and the app can "start"
    onDeviceReady: function() {
        // Cordova is now ready --- do remaining Cordova setup.
        console.log("onDeviceReady");
        // Button/Touch actions weren't setup in initialize()
        // because they will trigger Cordova specific actions
        refreshButton.ontouchstart = app.uiOnScan;
        refreshButton.disabled = false;
        deviceList.ontouchstart = app.uiOnConnect;
        disconnectButton.ontouchstart = app.uiOnDisconnect;
        var request = new XMLHttpRequest();
        request.open("GET", "test.txt");
        request.onreadystatechange = function() {//Call a function when the state changes.
            if (request.readyState == 4) {
                console.log("didit");
                app.testTextParse(request.responseText);
            }
        }
        request.send();
        // reader.readAsText(cordova.file.dataDirectory + '/test1.txt');
        app.uiOnScan();
    },
    testTextParse: function(value) {
        console.log("amhere");
        //var AaronCTFO = /[\s\n\t\r]*{([a-zA-Z0-9_]+):\s?([a-zA-Z0-9_]+)(?:,\s?([a-zA-Z0-9_]+))*(?:(;\s?[a-zA-Z0-9_]+):\s?([a-zA-Z0-9_]+)(?:,\s?([a-zA-Z0-9_]+))*)*}(?:[\s\t\n\r]*(![a-zA-Z0-9_\s]+):([a-zA-Z0-9_\s]*):\[([0-9]+),\s?([tf]),\s?([tf]),\s?([tf])\]:(?:[\s\n\t\r]*(?:(?:(SendTo)\(0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)\))|(?:(ReceiveValueFrom)\(0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)\))|(?:(ReceiveBetweenFrom)\(0x([0-9a-fA-F]+),\s?0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_])+\.([a-zA-Z0-9_]+)\))|(?:(ConditionalSendTo)\(0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)\))|(?:(ConditionalReceiveValueFrom)\(0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)\))|(?:(ConditionalReceiveBetweenFrom)\(0x([0-9a-fA-F]+),\s?0x([0-9a-fA-F]+),\s?([a-zA-Z0-9_])+\.([a-zA-Z0-9_]+)\)))[:;])+)+[\s\n\t\r]*/;
        var state = 0;
        var variables = {};
        var tests = {};
        var currentT;
        var currentS;
        var currentM;
        value = value.replace(/[ \s\n\r\t]*/g, "");
        var len = value.length;
        var j;
        console.log("value: " + value);
        console.log("length: " + len);
        // for(var i = 0; i < len; i++) {
        //     console.log("looping" + i);
        //     switch(state) {
        //         case 0:
        //             console.log('case0' + i);
        //             console.log('char at i: '+ value.charAt(i));
        //             if (value.slice(i).startsWith("{")) {
        //                 state = 1;
        //                 console.log("did");
        //             }
        //             console.log("whatdidyousee?");
        //             break;
        //         case 1:
        //             console.log('case1' + i);
        //             j = value.slice(i).search(/:/);
        //             currentS = value.slice(i, j);
        //             variables[currentS] = {};
        //             state = 2;
        //             i = j;
        //             break;
        //         case 2:
        //             console.log('case2' + i);
        //             j = value.slice(i).search(/,;}/);
        //             variables[currentS][value.slice(i, j)] = 0;
        //             i = j;
        //             if (value.charAt(j) == ";".charAt(0)){
        //                 state = 1;
        //             } else if (value.charAt(j) == "}".charAt(0)) {
        //                 i++;
        //                 state = 3;
        //             }
        //             break;
        //         case 3:
        //             console.log('case3' + i);
        //             j = value.slice(i).search(/:/);
        //             currentT = value.slice(i, j);
        //             tests[currentT] = {execs: []};
        //             i = j;
        //             state = 4;
        //             break;
        //         case 4:
        //             console.log('case4' + i);
        //             j = value.slice(i).search(/:/);
        //             tests[currentT]['description'] = value.slice(i, j);
        //             i = j;
        //             state = 5;
        //             break;
        //         case 5: 
        //             console.log('case5' + i);
        //             j = value.slice(i).search(/:/);
        //             tests[currentT]['casevals'] = value.slice(i, j);
        //             i = j;
        //             state = 6;
        //             break;
        //         case 6:
        //             console.log('case6' + i);
        //             j = value.slice(i).search(/(/);
        //             switch(value.slice(i, j)) {
        //                 case 'SendTo':
        //                     i = j+3;
        //                     j = value.slice(i).search(/:/);
        //                     currentM = value.slice(i, j);
        //                     break;
        //                 case 'ConditionalSendTo':
        //                     break;
        //                 case 'ReceiveValueFrom':
        //                     break;
        //                 case 'ConditionalReceiveValueFrom':
        //                     break;
        //                 case 'ReceiveBetweenFrom':
        //                     break;
        //                 case 'ConditionalReceiveBetweenFrom':
        //                     break;

        //             }
        //             break;
        //         default:
        //             console.log('default + i');
        //     }
        // }
        console.log("Variables: " + variables);
        console.log("Tests: " + tests);

    },






// **** Callbacks from the user interface.  These respond to UI events ****
    // TODO: Add Functions to handle the callbacks (events) on the new controls
    // (Pay close attention to the syntax of functions)

    // Start scanning (also called at startup)
    uiOnScan: function() {
        console.log("uiOnScan");
        console.log(app.testText);
        deviceList.innerHTML = ""; // clear the list at the start of a uiOnScan
        app.uiShowProgressIndicator("Scanning for Bluetooth Devices...");

        // Start the uiOnScan and setup the "callbacks"
        ble.startScan([],
            app.bleOnDeviceDiscovered,
            function() { alert("Listing Bluetooth Devices Failed"); }
        );

        // Stop uiOnScan after 5 seconds
        setTimeout(ble.stopScan, 5000, app.bleOnScanComplete);
    },

    // An item has been selected, TRY to connect
    uiOnConnect: function (e) {
        console.log("uiOnConnect");
        // Stop scanning
        ble.stopScan();
        // Retrieve the device ID from the HTML element.
        var device = e.target.dataset.deviceId;
        // Request the connection
        ble.connect(device, app.bleOnConnect, app.bleOnDisconnect);

        // Show the status
        app.uiShowProgressIndicator("Requesting connection to " + device);
    },

    // The user has hit the Disconnect button
    uiOnDisconnect: function (e) {
        console.log("uiOnDisconnect");
        if (e) {
            e.preventDefault();
        }

        app.uiSetStatus("Disconnecting...");
        ble.disconnect(app.connectedPeripheral.id, function() {
            app.uiSetStatus("Disconnected");
            setTimeout(app.uiOnScan, 800);
        });
        serviceList.innerHTML = " ";
        serviceList.hidden = "hidden";

    },



// **** Callbacks from the "ble" Object: These respond to BLE events
    bleOnDeviceDiscovered: function(device) {
        console.log("bleOnDeviceDiscovered");

        // Show the list of devices (if it isn't already shown)
        app.uiShowDeviceListScreen();

        console.log(JSON.stringify(device));

        // Add an item to the list

        // 1. Build the HTML element
        var listItem = document.createElement('li');  // Start list item (li)
        // Add a custom piece of data to the HTML item (so if the HTML item is selected it will
        // be possible to retrieve the device ID).
        listItem.dataset.deviceId = device.id;
        var rssi = "";
        if (device.rssi) {
            rssi = "RSSI: " + device.rssi + "<br/>";
        }
        listItem.innerHTML = device.name + "<br/>" + rssi + device.id;
        // 2. Add it to the list
        deviceList.appendChild(listItem);

        // Update the status
        var deviceListLength = deviceList.getElementsByTagName('li').length;
        app.uiSetStatus("Found " + deviceListLength +
                      " device" + (deviceListLength === 1 ? "." : "s."));
    },

    bleOnScanComplete: function() {
        console.log("bleOnScanComplete");
        var deviceListLength = deviceList.getElementsByTagName('li').length;
        if (deviceListLength === 0) {
            app.uiShowDeviceListScreen();
            app.uiSetStatus("No Bluetooth Peripherals Discovered.");
        }
    },

    // At the completion of a successful connection
    bleOnConnect: function(peripheral) {
        console.log("bleOnConnect");
        // Save the peripheral object for later use
        app.connectedPeripheral = peripheral;
        app.services = {};
        for (var i = 0; i < peripheral.services.length; i++) {
            app.services[peripheral.services[i]] = {};
        }
        for (var i = 0; i < peripheral.characteristics.length; i++) {
            app.services[peripheral.characteristics[i].service][peripheral.characteristics[i].characteristic] = peripheral.characteristics[i];
        }

        app.uiSetStatus("Connected");
        for(var service in app.services) {

            var listItem = document.createElement('li');  // Start list item (li)
        // Add a custom piece of data to the HTML item (so if the HTML item is selected it will
        // be possible to retrieve the device ID).
             listItem.textContent = service;
        // 2. Add it to the list
             serviceList.appendChild(listItem);
             var babylist = document.createElement('ul');
             listItem.appendChild(babylist);

            for(var characteristic in app.services[service]){

                var listItemlet = document.createElement('li');  // Start list item (li)
        // Add a custom piece of data to the HTML item (so if the HTML item is selected it will
        // be possible to retrieve the device ID).
                // listItemlet.Class = sub;
                listItemlet.textContent = characteristic;
        // 2. Add it to the list
                babylist.appendChild(listItemlet);
        
            }
        }
        app.uiShowServices()
        // TODO: When connected you can start notifications

    },
    // TODO: Create a function to call everytime the notification is "successful"


    bleOnDisconnect: function(reason) {
        console.log("bleOnDisconnect");
        if (!reason) {
            reason = "Connection Lost";
        }
        app.uiHideProgressIndicator();
        app.uiShowDeviceListScreen();
        app.uiSetStatus(reason);
        serviceList.innerHTML = " ";
        serviceList.hidden = "hidden";
    },

// ***** Functions that update the user interfaces
    uiShowProgressIndicator: function(message) {
        if (!message) { message = "Processing"; }
        progress.firstElementChild.innerHTML = message;
        progress.hidden = false;
        statusDiv.innerHTML = "";
    },

    uiHideProgressIndicator: function() {
        progress.hidden = true;
    },

    uiShowDeviceListScreen: function() {
        deviceListScreen.hidden = false;
        main.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
        serviceList.innerHTML = " ";
        serviceList.hidden = "hidden";
    },

    uiShowControlScreen: function() {
        deviceListScreen.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },

    uiShowServices: function() {
        deviceListScreen.hidden = true;
        serviceList.hidden = false;
        app.uiHideProgressIndicator();
           
        main.hidden = false;

    },

    uiSetStatus: function(message){
        console.log(message);
        statusDiv.innerHTML = message;
    },

// Utility function for writing data
    writeData: function(data, s_uuid, w_uuid) {
        console.log("Write");
        var success = function() {
            console.log("Write success");
        };

        var failure = function() {
            alert("Failed writing data");
        };
        ble.writeWithoutResponse(app.connectedPeripheral.id, s_uuid, w_uuid, data.buffer, success, failure);
    },
};

// When this code is loaded the app.initialize() function is called
// to start setting up the application logic.
app.initialize();