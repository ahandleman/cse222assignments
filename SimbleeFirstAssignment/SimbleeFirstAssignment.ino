// Notice the include is SimbleeBLE NOT SimbleForMobile
/* Written by Chloe Wright and Aaron Handleman
 * Writing Protocols:
 * Tag | Action
 * 0x01| (On/Off/Toggle) If the second byte is 0x01 turn bulb on, if the second byte is 0x02 turn bulb off, if the second byte is 0x03 toggle bulb.  Other values for the second byte do not have defined behavior.
 * 0x02| (Quickset Color) The second byte dictates color: 0x00 is none, 0x01 is White, 0x02 is Red, 0x03 is Green, 0x04 is Blue.
 * 0x03| (Hard set Color) The first byte sets Red, the second byte sets green, the third byte sets blue.  All values are 0-255.
 * 0x04| (Dim/Undim) The second byte sets the dim level that the app divides all three colors by to a value from 0-255.
 * 0x05| (Timer) This is four bytes long not including the tag, and is interpreted as an unsigned 4-byte integer. The first bit dictates whether the number defined by the remaining bits refer to the on timer or the off timer.  1 is the on timer, 0 is the off timer.  The time is in seconds.
 * 0x06| (Start-Stop-Timer) If the second byte is: 0x00, then the bulb pauses the off timer, 0x01, then the bulb starts the off timer, 0x80, then the bulb pauses the on timer, 0x81 then the bulb starts the on timer.
 * 0x07| (Request information) The second byte determines what data the bulb sends back, 0x01 requests the on/off state, 0x02 requests the current color, 0x03 requests the on timer current value, 0x04 requests the off timer current value, 0x05 requests the set fade duration, 0x06 requests the current dim level
 * 0x08| (Set Fade) The second byte sets the duration of the fade, it takes values from 0x00 to 0x0A, if it recieves a value higher than 0x0A then it sets a fade time of 0x0A.
 * 
 * 
 * Reading Protocols:
 * Tag | Action
 * 0x01| (On-Off) 0x00 if off, 0x01 if on
 * 0x02| (Current Color) Of the remaining 3 bytes, the first is red, the second is green, the third is blue.  All values are 0-255.
 * 0x03| (On-Timer) Sends the on-timer in seconds as a 4-byte int.
 * 0x04| (Off-Timer) Sends the off-timer in seconds as a 4-byte int.
 * 0x05| (Fade-Duration) Sends the fade duration in seconds as a 1-byte unsigned int
 * 0x06| (Dim Level) Sends the dim level as a 1-byte unsigned int
 */

#include <SimbleeBLE.h>
#define red_pin 2
#define green_pin 3
#define blue_pin 4

int red_set = 255;
int red_value = 255;
int red_old = 0;

int blue_set = 255;
int blue_value = 255;
int blue_old = 0;

int green_set = 255;
int green_value = 255;
int green_old = 0;

int dimLevel = 0x0A;

bool bulb_state = false;
char* outData;

unsigned long on_timer;
unsigned long acc_on;
unsigned long off_timer;
unsigned long acc_off;
bool on_timer_running;
bool off_timer_running;

unsigned int fade_set = 0x0A;
unsigned int fade_timer;
unsigned long acc_fade;

bool button1;
bool button2;

void setup() {
  // TODO: Change the "ledbtn" string to a unique 2-5 letter code for your group
  SimbleeBLE.advertisementData = "fffff";
  SimbleeBLE.deviceName= "ChloeAaron";
  Serial.begin(9600);
  pinMode(red_pin, OUTPUT);
  pinMode(green_pin, OUTPUT);
  pinMode(blue_pin, OUTPUT);
  pinMode(5, INPUT);
  pinMode(6, INPUT);
  // start the BLE stack
  SimbleeBLE.begin();
}

void loop() {
  Aaron_loop_checks(); //Runs Aaron's weird commands
  buttonTest(); //Checks the button
  controlOperations(); //runs the timer controls timers
  setBulb(); //sets the bulb output
}

void SimbleeBLE_onReceive(char data[], int len) {
  Serial.print("Data: ");
  for (int i = 0; i < len; i++) {
    Serial.print(i);
    Serial.print("=> ");
    Serial.print(((int)data[i]));
    Serial.print(", ");
  }
  Serial.println("");
  //If statement for 0x01 through 0x05 and the default case written as a group.
  switch (data[0]) {
    case 0x01:
      if (len >= 2) {
        switch (data[1]) {
          case 0x01:
            bulbOn();
            break;
          case 0x02:
            bulbOff();
            break;
          case 0x03:
            toggleBulb();
            break;
          default:
            break;
        }
      }
      break;
    case 0x02: //Quickset color.  0x01 is white 0x02 is red 0x03 is green 0x04 is blue
      if (len >= 2) {
        switch (data[1]) {
          case 0x01: //white
            red_set = 0xFF;
            green_set = 0xFF;
            blue_set = 0xFF;
            fadeStart();
            break;
          case 0x02: //red
            red_set = 0xFF;
            green_set = 0x00;
            blue_set = 0x00;
            fadeStart();
            break;
          case 0x03: //green
            red_set = 0x00;
            green_set = 0xFF;
            blue_set = 0x00;
            fadeStart();
            break;
          case 0x04: //blue
            red_set = 0x00;
            green_set = 0x00;
            blue_set = 0xFF;
            fadeStart();
            break;
          default:
            break;
        }
      }
      break;
    case 0x03: //hard sets color to value
      if (len >= 4) {
        red_set = data[1];
        green_set = data[2];
        blue_set = data[3];
        fadeStart();
      }
      break;
    case 0x04: // sets the dim level for the bulb
      if (len >= 2) {
        dimLevel = (data[1] != 0 ? data[1] : 1);
      }
      break;
    case 0x05: //sets the on timer or off timer
      if (len >= 5) {
        unsigned long i;
        unsigned long temp = data[1];
        i += (temp << 24);
        temp = data[2];
        i += (temp << 16);
        temp = data[3];
        i += (temp << 8);
        temp = data[4];
        i += temp;
        temp = i & 0x80000000;
        if (temp) {
          i -= temp;
          on_timer = i;
          on_timer_running = false;
        } else {
          off_timer = i;
          off_timer_running = false;
        }
      }
      break;
    case 0x06: // turns the on and off timers on and off
      if (len >= 2) {
        switch (data[1]) {
          case 0x00:
            off_timer_running = false;
            break;
          case 0x01:
            off_timer_running = true;
            break;
          case 0x80:
            on_timer_running = false;
            break;
          case 0x81:
            on_timer_running = true;
            break;
        }
      }
      break;
    case 0x07: //sends outputs if user requests them
      if (len >= 2) {
        switch (data[1]) {
          case 0x01:
            outData = new char[2];
            outData[0] = 0x01;
            outData[1] = bulb_state;
            SimbleeBLE.send(outData, 2);
            break;
          case 0x02:
            outData = new char[4];
            outData[0] = 0x02;
            outData[1] = red_value;
            outData[2] = blue_value;
            outData[3] = green_value;
            SimbleeBLE.send(outData, 4);
            break;
          case 0x03:
            outData = new char[5];
            outData[0] = 0x03;
            outData[1] = on_timer >> 24;
            outData[2] = on_timer >> 16;
            outData[3] = on_timer >> 8;
            outData[4] = on_timer;
            SimbleeBLE.send(outData, 5);
            break;
          case 0x04:
            outData = new char[5];
            outData[0] = 0x04;
            outData[1] = off_timer >> 24;
            outData[2] = off_timer >> 16;
            outData[3] = off_timer >> 8;
            outData[4] = off_timer;
            SimbleeBLE.send(outData, 5);
            break;
          case 0x05:
            outData = new char[2];
            outData[0] = 0x05;
            outData[1] = (fade_set / 5);
            SimbleeBLE.send(outData, 2);
            break;
          case 0x06:
            outData = new char[2];
            outData[0] = 0x06;
            outData[1] = dimLevel;
            SimbleeBLE.send(outData, 2);
            break;
          default:
            break;
        }
      }
      break;
    case 0x08:  //sets the timer on the fade, this starts a fade operation to protect against weird cases if it's currently fading
      if (len >= 2) {
        fade_set = 5 * (data[1] > 0x0A ? 0x0A : data[1]);
        fadeStart();
      }
      break;
    default: //This is a debugging tool
      for (int i = 0; i < len; i++) {
        char cur = data[i];
        Serial.print(cur, HEX);
        Serial.print(" ");
      }
      Serial.println(" ");
      break;
  }
}

void bulbOn() { //turns the bulb on and sends the current state
  bulb_state = true;
  outData = new char[2];
  outData[0] = 0x01;
  outData[1] = bulb_state;
  SimbleeBLE.send(outData, 2);
}

void bulbOff() { //turns the bulb off and sends the current state
  bulb_state = false;
  outData = new char[2];
  outData[0] = 0x01;
  outData[1] = bulb_state;
  SimbleeBLE.send(outData, 2);
}

void toggleBulb() { //toggles the bulb and sends the current state
  bulb_state = !bulb_state;
  outData = new char[2];
  outData[0] = 0x01;
  outData[1] = bulb_state;
  SimbleeBLE.send(outData, 2);
}

void setBulb() { //sets the analog writes based on the values
  if (bulb_state) {
    analogWrite(2, (red_value / dimLevel));
    analogWrite(3, (green_value / dimLevel));
    analogWrite(4, (blue_value / dimLevel));
  } else {
    analogWrite(2, 0);
    analogWrite(3, 0);
    analogWrite(4, 0);
  }
}
void fadeStart() { // sets the old values to the current setting, resets the timer, and starts the timer.
  red_old = red_value;
  green_old = green_value;
  blue_old = blue_value;
  fade_timer = fade_set;
}

void controlOperations() {  //runs the separate control functions
  fadeControl();
  onTimer();
  offTimer();
}

void fadeControl() {  //runs delta time to control the fade.
  if (millis() - acc_fade > 200) {
    acc_fade += 200;
    if (fade_timer != 0) {
      fade_timer--;  //sets the output to a point between the old and new determined by the fade timer and a linear difference between the two
      red_value = (int)(((((float)red_old) * fade_timer) + (((float) red_set) * (fade_set - fade_timer))) / ((float)fade_set));
      green_value = (int)(((((float)green_old) * fade_timer) + (((float) green_set) * (fade_set - fade_timer))) / ((float)fade_set));
      blue_value = (int)(((((float)blue_old) * fade_timer) + (((float) blue_set) * (fade_set - fade_timer))) / ((float)fade_set));
     
      red_value = (red_value > 0xFF ? 0xFF : red_value);
      green_value = (green_value > 0xFF ? 0xFF : green_value);
      blue_value = (blue_value > 0xFF ? 0xFF : blue_value);
      
      outData = new char[4];
      outData[0] = 0x02; //sends the message dictating what the colors are
      outData[1] = red_value;
      outData[2] = blue_value;
      outData[3] = green_value;
      SimbleeBLE.send(outData, 4);
    }
  }
}

void onTimer() { //runs delta time to manage the on timer
  if (millis() - acc_on > 1000) {
    acc_on += 1000;
    if (on_timer != 0) {
      if (on_timer_running) {
        on_timer--;
        if (on_timer == 0) {
          bulbOn();
        }
      }
    }
  }
}

void offTimer() {  //runs delta time to manage the off timer
  if (millis() - acc_off > 1000) {
    acc_off += 1000;
    if (off_timer != 0) {
      if (off_timer_running) {
        off_timer--;
        if (off_timer == 0) {
          bulbOff();
        }
      }
    }
  }
}

void buttonTest() {  //button checking and running bulb on/off
  int temp1 = digitalRead(5);
  int temp2 = digitalRead(6);  //Both from modified group code
  if (button1 != temp1) {  //Checks to see if the button has changed, so we don't continuously send packets when pressed
    button1 = temp1;
    if (button1 == 1) { //Only sends on press, not release
      bulbOff();
    }
  }
  if (button2 != temp2) {  //Checks to see if the button has changed, so we don't continuously send packets when pressed
    button2 = temp2;
    if (button2 == 1) { //Only sends on press, not release
      bulbOn();
    }
  }
}

void SimbleeBLE_onConnect()
{
  Serial.println("onConnect");

}

void SimbleeBLE_onDisconnect()
{
  Serial.println("onDisconnect");
}

bool Aaron_debug_on = false;
int Aaron_loop_checks() {
  if (Serial.available() <= 0) {
    return 0;
  }
  String s = "";
  while (Serial.available() > 0) {
    char mychar;
    mychar = (char)Serial.read();
    if (mychar != '\n') {
      s += mychar;
    }
  }
  s.toUpperCase();
  Aaron_debug("Input: " + s);
  switch (s[0]) {
    case 'R':
      Aaron_restart();
      break;
    case 'D':
      Aaron_debug_on = !Aaron_debug_on;
      String temp1 = "Debug is now ";
      temp1 += (Aaron_debug_on ? "on." : "off.");
      Serial.println(temp1);
      break;
  }
}

int Aaron_restart() {
  Aaron_debug("Restart");
  delay(100);
  Simblee_systemReset();
}

int Aaron_debug(char* s) {
  if (Aaron_debug_on) {
    Serial.println(s);
  }
}

int Aaron_debug(String s) {
  if (Aaron_debug_on) {
    Serial.print("ADB:  ");
    Serial.println(s);
  }
}

