

// Heavily based on Bluetooth Low Energy Lock (c) 2014-2015 Don Coleman
// See: https://github.com/MakeBluetooth/ble-lock/blob/master/phonegap/www/js/index.js

var SERVICE_UUID = 'FE84'; //'0000fe84-0000-1000-8000-00805f9b34fb';
var WRITE_UUID = '2d30c083-f39f-4ce6-923f-3484ea480596';
var READ_UUID = '2d30c082-f39f-4ce6-923f-3484ea480596';

//******   Utility functions (not used here yet) ******
function stringToArrayBuffer(str) {
    // assuming 8 bit bytes
    var ret = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        ret[i] = str.charCodeAt(i);
        console.log(ret[i]);
    }
    return ret.buffer;
}
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}


//******   Actual Application Class/Logic ******

// Note:   Calls to object methods use it's variable name (i.e., app.) rather than
//         "this".  JavaScript's "this" variable doesn't follow the same scope/value
//         rules as in less dynamic languages.  (The "this" is variable dynamically
//         refers to the object that calls code rather than the object in which the "this"
//         is used.  For programmers used to other languages the behavior of "this" in
//         callbacks is often unexpected. To avoid confusion and ambiguity, "this" isn't
//         used at all here.

// Naming Conventions used here:
//   User Interface:
//     Functions that update the user interface or process user interface events start with "ui"
//   Event Handlers:
//     Any code that "responds to an event" has a name that includes the word "on" or "On".
//     Functions that begin with "bleOn" are for BLE based events.
//     Functions that begin with "uiOn" are for user interface events (buttons/touches)

// BLE Object function documentation can be found at:
// https://github.com/don/cordova-plugin-ble-central#api

var app = {

    currentColor: "FFFFFF",

    // Initialized the app. Hide content / etc.
    initialize: function() {
        console.log("initialize");

        // Do initial screen configuration.
        // This can be done here because this file is loaded from the HTML file.
        deviceListScreen.hidden = false;
        app.rph = 0;
        app.gph = 0;
        app.bph = 0;
        lampControlPanel.hidden = true;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = true;
        bulbState.hidden = true;
        colorQuickSetPanel.hidden = true;
        timerControlPanel.hidden = true;
        fadePanel.hidden = true;
        // Disable the refresh button until the app is completely ready
        refreshButton.disabled = true;

        // Register to be notified when the "device is ready"
        // This delays the execution of any more code until all the Cordova code is loaded.
        // (This file may be loaded before the Cordova.js file is loaded and, consequently,
        //  shouldn't use any of Cordova's features)
        // See: http://cordova.apache.org/docs/en/6.x/cordova/events/events.html#page-toc-source
        document.addEventListener('deviceready', app.onDeviceReady, false);
    },

// **** Callbacks for application "lifecycle" events. These respond to significant events when the App runs ******

    // the device is ready and the app can "start"
    onDeviceReady: function() {
        // Cordova is now ready --- do remaining Cordova setup.
        console.log("onDeviceReady");

        // Button/Touch actions weren't setup in initialize()
        // because they will trigger Cordova specific actions
        refreshButton.ontouchstart = app.uiOnScan;
        refreshButton.disabled = false;
        deviceList.ontouchstart = app.uiOnConnect;
        onButton.ontouchstart = app.uiOnLampOn;
	    offButton.ontouchstart = app.uiOnLampOff;
        disconnectButton.ontouchstart = app.uiOnDisconnect;
        red.onchange = app.uiOnColorChange;
        green.onchange = app.uiOnColorChange;
        blue.onchange = app.uiOnColorChange;

        setColorsButton.ontouchstart = app.uiShowSliderScreen;
        quickSetColorsButton.ontouchstart = app.uiShowQuickSetScreen;
        setTimerButton.ontouchstart = app.uiShowTimerScreen;
        returnButton.ontouchstart = app.uiShowControlScreen;
        setFadeButton.ontouchstart = app.uiShowFadeScreen;

         whiteQS.ontouchstart = function(){app.uiOnQuickColorChange(1);};
         redQS.ontouchstart =  function(){app.uiOnQuickColorChange(2);};
         greenQS.ontouchstart =  function(){app.uiOnQuickColorChange(3);};
         blueQS.ontouchstart =  function(){app.uiOnQuickColorChange(4);};

         on_timer_start.ontouchstart = function(){app.uiTimerSend(0x81)};
         on_timer_pause.ontouchstart = function(){app.uiTimerSend(0x80)};
         off_timer_start.ontouchstart = function(){app.uiTimerSend(0x01)};
         off_timer_pause.ontouchstart = function(){app.uiTimerSend(0x00)};

         on_timer_set.onchange = function(){app.uiTimerSetSend(parseInt(on_timer_set.value), true)};
         off_timer_set.onchange = function(){app.uiTimerSetSend(parseInt(off_timer_set.value), false)};
         fadeField.onchange = function(){app.uiFadeSend(fadeField.value);};
        // TODO: Configure the new controls so they will "call" a function when an event happens

        // Call the uiOnScan function to automatically start scanning
        app.uiOnScan();
    },



// **** Callbacks from the user interface.  These respond to UI events ****
    // TODO: Add Functions to handle the callbacks (events) on the new controls
    // (Pay close attention to the syntax of functions)

    // Start scanning (also called at startup)
    uiOnScan: function() {
        console.log("uiOnScan");

        deviceList.innerHTML = ""; // clear the list at the start of a uiOnScan
        app.uiShowProgressIndicator("Scanning for Bluetooth Devices...");

        // Start the uiOnScan and setup the "callbacks"
        ble.startScan([],
            app.bleOnDeviceDiscovered,
            function() { alert("Listing Bluetooth Devices Failed"); }
        );

        // Stop uiOnScan after 5 seconds
        setTimeout(ble.stopScan, 5000, app.bleOnScanComplete);
    },

    // An item has been selected, TRY to connect
    uiOnConnect: function (e) {
        console.log("uiOnConnect");
        // Stop scanning
        ble.stopScan();

        // Retrieve the device ID from the HTML element.
        var device = e.target.dataset.deviceId;
        // Request the connection
        ble.connect(device, app.bleOnConnect, app.bleOnDisconnect);

        // Show the status
        app.uiShowProgressIndicator("Requesting connection to " + device);
    },

    // The user has hit the Disconnect button
    uiOnDisconnect: function (e) {
        console.log("uiOnDisconnect");
        if (e) {
            e.preventDefault();
        }

        app.uiSetStatus("Disconnecting...");
        ble.disconnect(app.connectedPeripheral.id, function() {
            app.uiSetStatus("Disconnected");
            setTimeout(app.uiOnScan, 800);
        });
    },

    uiOnLampOn: function() {
        console.log("uiOnLampOn");
        var data = new Uint8Array(2);
        data[0] = 0x1;
        data[1] = 0x1;
        app.writeData(data);
    },

    uiOnLampOff: function() {
        console.log("uiOnLampOff");
        var data = new Uint8Array(2);
        data[0] = 0x1;
        data[1] = 0x2;
        app.writeData(data);
    },

    uiTimerSend: function(timer_num) {
        var data = new Uint8Array(2);
        data[0] = 6;
        data[1] = timer_num;
        app.writeData(data);
    },

    uiFadeSend: function(time) {
        var data = new Uint8Array(2);
        data[0] = 8;
        data[1] = time;
        app.writeData(data);
    },

    uiTimerSetSend: function(time, on_timer) {
        var data = new Uint8Array(5);
        data[0] = 5;
        console.log("time");
        var this_string = ("00000000" + time.toString(16)).slice(-8);
        console.log(this_string);
        data[1] = (parseInt(this_string.substring(0,2), 16) + (on_timer ? 0x80 : 0x00));
        data[2] = parseInt(this_string.substring(2,4), 16);
        data[3] = parseInt(this_string.substring(4,6), 16);
        data[4] = parseInt(this_string.substring(6,8), 16);
        app.writeData(data);
    },

    uiOnColorChange: function() {
        console.log("uiOnColorChange");
        var data = new Uint8Array(4);
        data[0] = 3;
        data[1] = red.value;
        data[2] = green.value;
        data[3] = blue.value;

        app.writeData(data);
    },

    uiSetOnTimerDisplay: function(time) {
        on_timer_value.textContent = time + " seconds";
    },

    uiSetOffTimerDisplay: function(time) {
        off_timer_value.textContent = time + " seconds";
    },

    uiSetFadeDisplay: function(time) {
        fade_value.textContent = time + " seconds";
    },

    uiOnQuickColorChange: function(color){
        console.log("uiOnQuickColorChange");
        var data = new Uint8Array(2);
        data[0] = 2;
        data[1] = color;
        app.writeData(data);
    },

// **** Callbacks from the "ble" Object: These respond to BLE events
    bleOnDeviceDiscovered: function(device) {
        console.log("bleOnDeviceDiscovered");

        // Show the list of devices (if it isn't already shown)
        app.uiShowDeviceListScreen();

        console.log(JSON.stringify(device));

        // Add an item to the list

        // 1. Build the HTML element
        var listItem = document.createElement('li');  // Start list item (li)
        // Add a custom piece of data to the HTML item (so if the HTML item is selected it will
        // be possible to retrieve the device ID).
        listItem.dataset.deviceId = device.id;
        var rssi = "";
        if (device.rssi) {
            rssi = "RSSI: " + device.rssi + "<br/>";
        }
        listItem.innerHTML = device.name + "<br/>" + rssi + device.id;
        // 2. Add it to the list
        deviceList.appendChild(listItem);

        // Update the status
        var deviceListLength = deviceList.getElementsByTagName('li').length;
        app.uiSetStatus("Found " + deviceListLength +
                      " device" + (deviceListLength === 1 ? "." : "s."));
    },

    bleOnScanComplete: function() {
        console.log("bleOnScanComplete");
        var deviceListLength = deviceList.getElementsByTagName('li').length;
        if (deviceListLength === 0) {
            app.uiShowDeviceListScreen();
            app.uiSetStatus("No Bluetooth Peripherals Discovered.");
        }
    },

    // At the completion of a successful connection
    bleOnConnect: function(peripheral) {
        console.log("bleOnConnect");
        // Save the peripheral object for later use
        ble.startNotification(peripheral.id, SERVICE_UUID, READ_UUID, app.bleReadMessage);
        app.connectedPeripheral = peripheral;
        app.uiShowControlScreen();
        app.uiSetStatus("Connected");
        // TODO: When connected you can start notifications
        
        for (var i = 1; i < 7; i++) {
            var data = new Uint8Array(2);
            data[0] = 0x07;
            data[1] = i;
            app.writeData(data);
        }
        
    },
    // TODO: Create a function to call everytime the notification is "successful"

    bleReadMessage: function(buffer) {
        var data = new Uint8Array(buffer);
        console.log("read");
        console.log(data);
        switch(data[0]) {
            case 0x01:
                app.uiSetOnOffDisplay(data[1]);
            break;
            case 0x02:
                app.rph = data[1];
                app.gph = data[2];
                app.bph = data[3];
                app.currentColor = (("00" + data[1].toString(16)).slice(-2) + ("00" + data[2].toString(16)).slice(-2) + ("00" + data[3].toString(16)).slice(-2));
                app.uiSetColorDisplay();
                console.log(app.currentColor);
            break;
            case 0x03:
                console.log("reading on timer");
                var tstr = ("00" + data[1].toString(16)).slice(-2) + ("00" + data[2].toString(16)).slice(-2) + ("00" + data[3].toString(16)).slice(-2) + ("00" + data[4].toString(16)).slice(-2);
                var tint = (parseInt(tstr, 16) %  2147483648);
                app.uiSetOnTimerDisplay(tint);
                console.log(tint);
            break;
            case 0x04:
                console.log("reading off timer");
                var tstr = data[1].toString(16) + data[2].toString(16) + data[3].toString(16) + data[4].toString(16);
                var tint = (parseInt(tstr, 16) %  2147483648);
                app.uiSetOffTimerDisplay(tint);
                console.log(tint);
            break;
            case 0x05:
                app.uiSetFadeDisplay(data[1]);
            break;
        }
    },

    bleOnDisconnect: function(reason) {
        console.log("bleOnDisconnect");
        if (!reason) {
            reason = "Connection Lost";
        }
        app.uiHideProgressIndicator();
        app.uiShowDeviceListScreen();
        app.uiSetStatus(reason);
        ble.stopNotification(peripheral.id, SERVICE_UUID, READ_UUID);
    },

// ***** Functions that update the user interfaces
    uiShowProgressIndicator: function(message) {
        if (!message) { message = "Processing"; }
        progress.firstElementChild.innerHTML = message;
        progress.hidden = false;
        statusDiv.innerHTML = "";
    },

    uiHideProgressIndicator: function() {
        progress.hidden = true;
    },

    uiShowDeviceListScreen: function() {
        deviceListScreen.hidden = false;
        bulbState.hidden = true;
        lampControlPanel.hidden = true;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = true;
        colorQuickSetPanel.hidden = true;
        fadePanel.hidden = true;
        timerControlPanel.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },

    uiShowControlScreen: function() {
        deviceListScreen.hidden = true;
        bulbState.hidden = false;
        lampControlPanel.hidden = false;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = true;
        colorQuickSetPanel.hidden = true;
        fadePanel.hidden = true;
        timerControlPanel.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },
    uiShowQuickSetScreen: function() {
        deviceListScreen.hidden = true;
        bulbState.hidden = false;
        lampControlPanel.hidden = true;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = false;
        colorQuickSetPanel.hidden = false;
        timerControlPanel.hidden = true;
        fadePanel.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },
    uiShowSliderScreen: function() {
        red.value = app.rph;
        green.value = app.gph;
        blue.value = app.bph;

        deviceListScreen.hidden = true;
        lampControlPanel.hidden = true;
        bulbState.hidden = false;
        colorSlidersPanel.hidden = false;
        returnPanel.hidden = false;
        colorQuickSetPanel.hidden = true;
        timerControlPanel.hidden = true;
        fadePanel.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },
    uiShowTimerScreen: function() {
        deviceListScreen.hidden = true;
        lampControlPanel.hidden = true;
        bulbState.hidden = false;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = false;
        colorQuickSetPanel.hidden = true;
        timerControlPanel.hidden = false;
        fadePanel.hidden = true;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },

    uiShowFadeScreen: function() {
        deviceListScreen.hidden = true;
        lampControlPanel.hidden = true;
        bulbState.hidden = false;
        colorSlidersPanel.hidden = true;
        returnPanel.hidden = false;
        colorQuickSetPanel.hidden = true;
        timerControlPanel.hidden = true;
        fadePanel.hidden = false;
        app.uiHideProgressIndicator();
        statusDiv.innerHTML = "";
    },

    uiSetStatus: function(message){
        console.log(message);
        statusDiv.innerHTML = message;
    },

    uiSetOnOffDisplay: function(on){
        if(!on){
            bulbState.style.backgroundColor = "#000000";
            app.currentState = false;
        }
        else{
            bulbState.style.backgroundColor = "#" + app.currentColor;
            app.currentState = true;
        }
    },

    uiSetColorDisplay: function(){
        if(app.currentState ){
            bulbState.style.backgroundColor = "#" + app.currentColor;
        }
            bulbState.style.borderColor = "#" + app.currentColor;
    },

// Utility function for writing data
    writeData: function(data) {
        console.log("Write");
        var success = function() {
            console.log("Write success");
            console.log("data: " + data);
        };

        var failure = function() {
            alert("Failed writing data");
            console.log("data: " + data);

        };
        ble.writeWithoutResponse(app.connectedPeripheral.id, SERVICE_UUID, WRITE_UUID, data.buffer, success, failure);
    },
};
console.log("workyworky");
// When this code is loaded the app.initialize() function is called
// to start setting up the application logic.
app.initialize();