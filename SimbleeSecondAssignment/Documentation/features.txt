It shows the current state (on/off) and lets us set it.
It lets the user to both see and set the current color being used, regardless of whether the lamp is on, and quickset it.
It allows the user to see the current �on timer�, set it, and start it.
It allows the user to see the current �off timer�, set it, and start it.
It allows the user to control the lights fading, and see what it's currently set to.